#include <iostream>
#include <cmath>
#include <any>
int main()
{
  std::cout << "Hello World!" << std::endl;
  float bob{8.701};
  std::cout << bob << " rounds up to " << std::ceil(bob) << std::endl;
  // C++17 syntax
  std::any ben{16};
  float bill = std::any_cast<float>(ben);
  std::cout << "Square root of " << bill << " is " << std::sqrt(bill) << std::endl;
  return 0;
}
